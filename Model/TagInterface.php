<?php

namespace BetaMFD\TagBundle\Model;

interface TagInterface
{
    public function __toString();

    /**
     * Get the value of Id
     *
     * @return string
     */
    public function getId(): string;

    /**
     * Set the value of Id
     *
     * @param string id
     *
     * @return self
     */
    public function setId($id): Tag;


    /**
     * Get the value of Id
     *
     * @return string
     */
    public function getTag(): string;

    /**
     * Set the value of Id
     *
     * @param string id
     *
     * @return self
     */
    public function setTag($id): Tag;
}
