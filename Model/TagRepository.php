<?php

namespace BetaMFD\TagBundle\Model;

use BetaMFD\TagBundle\Model\TagRepositoryInterface;

/**
 * TagRepository
 */
class TagRepository
    extends \Doctrine\ORM\EntityRepository
    implements TagRepositoryInterface
{
}
