<?php

namespace BetaMFD\TagBundle\Model;

use Doctrine\ORM\Mapping as ORM;

abstract class Tag implements TagInterface
{
    protected  $id;

    public function __toString()
    {
        return $this->id;
    }

    /**
     * Get the value of Id
     *
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param string id
     *
     * @return self
     */
    public function setId($id): Tag
    {
        $this->id = $id;

        return $this;
    }


    /**
     * Get the value of Id
     *
     * @return string
     */
    public function getTag(): string
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param string id
     *
     * @return self
     */
    public function setTag($id): Tag
    {
        $this->id = $id;

        return $this;
    }
}
