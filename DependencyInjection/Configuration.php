<?php

namespace BetaMFD\TagBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('beta_mfd_file_handler');
        if (method_exists($treeBuilder, 'getRootNode')) {
            $rootNode = $treeBuilder->getRootNode();
        } else {
            $rootNode = $treeBuilder->root('beta_mfd_file_handler');
        }

        $rootNode
            ->children()
                ->scalarNode('tag_entity')
                ->defaultValue('App/Entity/Tag')
            ->end()
        ;

        return $treeBuilder;
    }
}
