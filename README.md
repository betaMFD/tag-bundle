This is a very simple tag service and an entity model to handle tags in other entities. 


### Composer Install ###

```
composer require betamfd/tag-bundle
```


### Enable the Bundle

Enable the bundle by adding it to the list of registered bundles
in the `app/AppKernel.php` file of your project:

```php
    new BetaMFD\TagBundle\BetaMFDTagBundle(),
```

Or if you're using a newer version of Symfony, enable it in the `config/bundles.php` file of your project:

```php
    BetaMFD\TagBundle\BetaMFDTagBundle::class => ['all' => true],
```


#### Add Entities

Create a Tag Entity and extend it from the model.
Create an ID field. 


```
<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="tag")
 * @ORM\Entity(repositoryClass="\BetaMFD\TagBundle\Model\TagRepository")
 */
class Tag extends \BetaMFD\TagBundle\Model\Tag
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="string", length=250, nullable=false)
     */
    protected $id;

    //getters/setters already created, gets/sets $id, and $tag which is the $id
    // __toString returns $id
    //any other fields or logic you want
    
    // You must make a construct
    // suggested __construct: 
    public function __construct($tag)
    {
        $this->id = $tag;
    }
}

```

If you want to add repository logic, you can extend the bundle's repository and replace it in your entity.
```
<?php

namespace App\Entity;

class FileRepository extends \BetaMFD\TagBundle\Model\TagRepository
{
  // any custom logic you want
}
```




### Using the Tag Service

In your controller, inject `\BetaMFD\TagBundle\Service\TagService`
