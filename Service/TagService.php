<?php

namespace BetaMFD\TagBundle\Service;


use BetaMFD\TagBundle\Model\Tag;

/**
 * This service takes care of the heavy lifting for adding/removing tags to entities
 *
 * Adding tags to a new entity takes a small amount of setup.
 *  1. Connect the entity to Tag using ManyToMany. Variable must be named $tags
 *   EXAMPLE:
 *      * @ORM\ManyToMany(targetEntity="App\Entity\Tag", inversedBy="items", cascade={"persist"})
 *      * @ORM\JoinTable(name="pantry_items_tags")
 *   private $tags;
 *
 *      * @ORM\ManyToMany(targetEntity="App\Entity\Pantry\Item", mappedBy="tags")
 *   private $items;
 *
 *  2. Generate getters/setters in the entities and add/remove functions in the Tag entity
 *     Add/remove is handled in the new entity in the Tag trait
 *
 *  3. Add trait to entity to bring in functions
 *       use \App\Entity\Traits\Tags;
 *
 *  4. To add a tag list to your form(s):
 *       $item = $builder->getData();
 *       $tag_list = $item->getTagString();
 *
 *       $builder->add('tag_list', TextType::class, [
 *           'label' => 'Add Tags',
 *           'help' => 'use a comma between tags',
 *           'mapped' => false,
 *           'data' => $tag_list,
 *       ]);
 *
 *  5. To handle the tag data you need to inject this service into your controller:
 *    private $tagService;
 *    public function __construct(\App\Service\TagService $tagService)
 *    {
 *        $this->tagService = $tagService;
 *    }
 *
 *  6. When you handle the form, use this service to handle the tag list:
 *     $tags = $form['tag_list']->getData();
 *     $this->tagService->setTagsFromString($tags);
 *     $this->tagService->addTagsToEntity($item);
 *
 */
class TagService
{
    private $em;
    private $tags;

    public function __construct(
        \Doctrine\ORM\EntityManagerInterface $entityManager
        )
    {
        $this->em = $entityManager;
    }

    public function setTagsFromString($string_of_tags)
    {
        if (!empty($string_of_tags)) {
            //remove unnecessary spaces after commas so I can just replace commas
            $string = str_replace(', ', ',', $string_of_tags);
            //explode on commas
            $tags = explode(',', $string);
            $this->tags = $tags;
        }
        return $this;
    }

    public function addTagsToEntity($entity, $clear = true)
    {
        $this->em->persist($entity);
        $entity->clearTags();
        foreach ($this->tags as $tag) {
            if (!empty($tag)) {
                $entity_tag = $this->em->getRepository('App:Tag')->find($tag);
                if (empty($entity_tag)) {
                    $entity_tag = new Tag($tag);
                    $this->em->persist($entity_tag);
                }
                $entity->addTag($entity_tag);
            }
        }
        return $this;
    }


}
